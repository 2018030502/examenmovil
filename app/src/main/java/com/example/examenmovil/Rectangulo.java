package com.example.examenmovil;

public class Rectangulo {

        private int base;
        private int altura;

        public Rectangulo(int base, int altura) {
            this.base = base;
            this.altura = altura;
        }

        public int rectangulo() {
            return base * altura;
        }

        public float calcularArea() {
            return base * altura;
        }

        public float calcularPerimetro() {
            return 2 * (base + altura);
        }


}
