package com.example.examenmovil;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examenmovil.R;
import com.example.examenmovil.Rectangulo;

public class CalcularRectangulo extends AppCompatActivity {

    private EditText etBase, etAltura;
    private TextView tvArea, tvPerimetro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular_rectangulo);

        etBase = findViewById(R.id.etBase);
        etAltura = findViewById(R.id.etAltura);
        tvArea = findViewById(R.id.tvArea);
        tvPerimetro = findViewById(R.id.tvPerimetro);

        Button btnCalcular = findViewById(R.id.btnCalcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        Button btnRegresar = findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogoSalir();
            }
        });
    }

    private void calcular() {
        String baseText = etBase.getText().toString().trim();
        String alturaText = etAltura.getText().toString().trim();

        if (!baseText.isEmpty() && !alturaText.isEmpty()) {
            int base = Integer.parseInt(baseText);
            int altura = Integer.parseInt(alturaText);

            Rectangulo rectangulo = new Rectangulo(base, altura);

            int area = Math.round(rectangulo.calcularArea());
            int perimetro = Math.round(rectangulo.calcularPerimetro());

            tvArea.setText(String.valueOf(area));
            tvPerimetro.setText(String.valueOf(perimetro));
        } else {
            Toast.makeText(this, "Por favor, ingresa los valores de base y altura.", Toast.LENGTH_SHORT).show();
        }
    }


    private void limpiar() {
        etBase.setText("");
        etAltura.setText("");
        tvArea.setText("");
        tvPerimetro.setText("");
    }

    private void mostrarDialogoSalir() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmar salida");
        builder.setMessage("¿Estás seguro de que deseas salir?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }
}
